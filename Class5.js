let class5 = {
    traversingTheDom : {
        getNextElementAfterBody: function(){
            let body = document.body;
            let nextElement = body.children;
            console.log(`first element is ${nextElement[0].localName} with a node type of ${nextElement[0].nodeType}`);
        },
        getBodyFromUL:function(){
            let ul = document.getElementsByTagName('ul');
            let body = ul[0].parentElement.parentElement;
            console.log(`Parente element of <ul> is ${body.localName}`);
        },
        getLiFromPElement:function(){
            let li = document.getElementsByTagName('p')[0].previousElementSibling.children[2];
            console.log(`Third li variable = ${li.localName} with id of ${li.id}`);
        }
        
    },
    productCrud:{
        insertAElement:function(){
            let parentOfP = document.querySelector('p:last-child').parentElement;
            let text = document.createTextNode('Buy Now!');

            let a = document.createElement('a');
            a.setAttribute('id','cta');
            a.appendChild(text);

            parentOfP.appendChild(a);
        },
        getDataColorOfImg:function(){
            let img = document.getElementsByTagName('img');
            console.log(`the data color is ${img[0].dataset.color}`);
        },
        setClassNameToHighlight:function(){
            let li = document.getElementsByTagName('li');
            li[2].className = 'highlight';
        },
        removeLastP:function(){
            let parentOfP = document.querySelector('p:last-child').parentElement;
            parentOfP.removeChild(document.querySelector('p:last-child'));
           
        }
        
    },
    plusMinus:{
        textValue: 0,
        addToValue:function(){
            this.textValue++;
            document.getElementById('numberDisplay').value = this.textValue;
        },
        minusToValue:function(){
            this.textValue--;
            document.getElementById('numberDisplay').value = this.textValue;
        }
        
    }
}



