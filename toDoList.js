// If an li element is clicked, toggle the class "done" on the <li>



// If a delete link is clicked, delete the li element / remove from the DOM



// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>



// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
    e.preventDefault();
    const input = this.parentNode.getElementsByTagName('input')[0];
    const text = input.value; // use this text to create a new <li>
    
    // Finish function here
  }
  
  // Add this as a listener to the two Add links

 document.addEventListener('click', function(e){
     if(e.target.tagName.toLowerCase() === 'a'){
         if(e.target.classList.contains('toToday')){
            e.target.classList.replace('toToday','toLater');
            e.target.innerText = 'Move to Later';
            let parent = e.target.parentNode;   
            todayList.appendChild(parent);
            
         }else if(e.target.classList.contains('toLater')){
            e.target.classList.replace('toLater','toToday');
            e.target.innerText = 'Move to Today';
            let parent = e.target.parentNode;  
            laterList.appendChild(parent); 
            
         }else if(e.target.classList.contains('delete')){
             let parent = e.target.parentNode;
             let grandParent = parent.parentNode;
             grandParent.removeChild(parent);
         }
         else if(e.target.classList.contains('add-item')){
            let text = e.target.previousElementSibling.value;
            if(!text){
                alert('Please enter a to do item');
                return;
            }
            let grandParent = e.target.parentNode.parentNode;
            if(grandParent.classList.contains('today')){
                let li = newToDo('today', text);
                todayList.appendChild(li);
            }else if(grandParent.classList.contains('later')){
                let li = newToDo('later', text);
                laterList.appendChild(li);
            }
         }
        
        
     }else if(e.target.tagName.toLowerCase() === 'span'){
        let parent = e.target.parentNode;  
        parent.className !== 'done' ? parent.className = 'done' : parent.className = '';
     }
 });

 let newToDo = function(tmp, text){
    let li = document.createElement('li');
    let span = document.createElement('span');
    span.innerText = text;

    let moveButton = document.createElement('a');
    moveButton.classList.add('move');

    let deleteButton = document.createElement('a');
    deleteButton.classList.add('delete');
    deleteButton.innerText = 'Delete';

     if(tmp === 'today'){
         moveButton.classList.add('toLater');
         moveButton.innerText = 'Move to Later';
     }else if(tmp === 'later'){
        moveButton.classList.add('toToday');
        moveButton.innerText = 'Move to Today';
     }
     li.appendChild(span);
     li.appendChild(moveButton);
     li.appendChild(deleteButton);
     return li;
 }
 let todayList = document.getElementsByClassName('today-list')[0];
 let laterList = document.getElementsByClassName('later-list')[0];